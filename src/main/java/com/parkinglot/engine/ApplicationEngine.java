package com.parkinglot.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

public class ApplicationEngine {

    public static void main(String[] args) {

    // If the commands are from command line or property file
    switch (args.length){
        //Command line
        case 0:{

            Scanner s = new Scanner(System.in);
            while (s.hasNext()){
                String input = s.nextLine();

                if (InputValidator.validate(input.split(" ")[0])){

                    Execution.execute(input);
                }else {
                    System.out.println("Invalid input");
                }
            }
            break;
        }

        //Read from Property File
        case 1:{
            int lineNo = 1;
            try {
                String input ="";
                ClassLoader classLoader = new ApplicationEngine().getClass().getClassLoader();
                File file = new File(classLoader.getResource(args[0]).getFile());
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                while ((input = bufferedReader.readLine()) != null){
                    input = input.trim();

                    if (InputValidator.validate(input.split(" ")[0])){
                        Execution.execute(input);
                    }else {
                        System.out.println("Invalid input");
                    }
                }
            }catch (Exception e){
                System.out.println("Input file exception at line no: " + lineNo);
            }

            break;
        }
        default:
            System.out.println("Invalid Input");
    }
    }
}
