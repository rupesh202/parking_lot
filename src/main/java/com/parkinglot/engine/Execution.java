package com.parkinglot.engine;
import com.parkinglot.Constant.Constants;
import com.parkinglot.model.Car;
import com.parkinglot.service.ApplicationService;
import com.parkinglot.service.ApplicationServiceImp;

public class Execution {
    static ApplicationService applicationService = new ApplicationServiceImp();

    public static void execute(String input){
        switch (input.split(" ")[0]){

            case Constants.CREATE_PARKING_LOT:{

                int capacity = Integer.parseInt(input.split(" ")[1]);
                applicationService.createParkingLot(capacity);
                break;
            }
            case Constants.PARK:{
                String registrationNumber = input.split(" ")[1];
                String color = input.split(" ")[2];
                applicationService.addCar(new Car(registrationNumber,color));
                break;
            }
            case Constants.LEAVE:{
                int slotNumber = Integer.parseInt(input.split(" ")[1]);
                applicationService.exitCar(slotNumber);
                break;
            }
            case Constants.REG_NUM_OF_CARS_WITH_COLOR:{
                String color = input.split(" ")[1];
                applicationService.regNumOfAllCarWithColor(color);
                break;
            }
            case Constants.SLOT_OF_CARS_WITH_COLOR:{
                String color = input.split(" ")[1];
                applicationService.slotOfParticularColor(color);
                break;
            }
            case Constants.SLOT_OF_REG_NUM:{
                String regNum = input.split(" ")[1];
                applicationService.slotOfParkedCar(regNum);
                break;
            }
            case Constants.STATUS:{
                applicationService.status();
                break;
            }
        }


    }
}
