package com.parkinglot.engine;
import com.parkinglot.Constant.Constants;

class  InputValidator {
    public static Boolean validate(String input){

        Boolean valid = false;
        switch (input){

            case Constants.CREATE_PARKING_LOT :{
                valid =true;
                break;
            }
            case Constants.LEAVE:{
                valid = true;
                break;
            }
            case Constants.PARK:{
                valid = true;
                break;
            }
            case Constants.REG_NUM_OF_CARS_WITH_COLOR:{
                valid = true;
                break;
            }
            case Constants.SLOT_OF_CARS_WITH_COLOR:{
                valid =true;
                break;
            }
            case Constants.SLOT_OF_REG_NUM:{
                valid =true;
                break;
            }
            case Constants.STATUS:{
                valid =true;
                break;
            }

        }
        return valid;
    }

}
