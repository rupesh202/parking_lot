package com.parkinglot.service;
import com.parkinglot.model.Car;

public interface  ApplicationService {
     void createParkingLot(int capacity);
     void addCar(Car car);
     void exitCar(int slotNumber);
     void regNumOfAllCarWithColor(String color);
     void slotOfParkedCar(String regNum);
     void slotOfParticularColor(String color);
     void status();
}
