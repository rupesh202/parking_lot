package com.parkinglot.service;

import com.parkinglot.model.Car;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.TreeSet;

public class ApplicationServiceImp  implements ApplicationService{

    ArrayList<Car> parkingSlots;
    TreeSet<Integer> freeSlots;
    Boolean found =false;

    @Override
    public void createParkingLot(int capacity) {

        if (parkingSlots != null){
            System.out.println("Parking Slot already created");
        }else {
            parkingSlots = new ArrayList<>(capacity);
            freeSlots = new TreeSet<>();
            //Initialize freeSlots
            for(int i=0; i<capacity; i++){
                freeSlots.add(i);
                parkingSlots.add(null);
            }
            System.out.println("Created a parking lot with " + capacity + " slots");
        }
    }

    @Override
    public void addCar(Car car) {

        if (parkingSlots != null){
            //First get the nearest free slot
            int slotNumber =0;
            try{
                slotNumber = freeSlots.first();
                freeSlots.remove(slotNumber);
                parkingSlots.set(slotNumber,car);
                System.out.println("Allocated slot number: " + slotNumber);

            }catch (NoSuchElementException e){
                System.out.println("No Parking slot found");
            }
        }else System.out.println("Parking lot not found");


    }

    @Override
    public void exitCar(int slotNumber) {

        if (parkingSlots !=null){

            if (parkingSlots.get(slotNumber) != null){
                parkingSlots.set(slotNumber,null);
                freeSlots.add(slotNumber);
                System.out.println("Slot number " + slotNumber + " is free");
            }else System.out.println("Slot " + slotNumber + " is not occupied");

        }else {
            System.out.println("Parking lot not created");
        }

    }

    @Override
    public void regNumOfAllCarWithColor(String color) {

        if (parkingSlots !=null){
            StringBuffer sb = new StringBuffer();
            for (int i=0; i<parkingSlots.size(); i++){
                if (parkingSlots.get(i) != null && parkingSlots.get(i).getColor().equalsIgnoreCase(color)){
                    found =true;
                    sb.append(parkingSlots.get(i).getRegistrationNumber() + " ");
                }
            }
            if (found) System.out.println(sb.toString());
            else System.out.println("Not found");
        }else System.out.println("parking lot not created");

    }

    @Override
    public void slotOfParkedCar(String regNum) {

        if (parkingSlots != null){
            StringBuffer sb = new StringBuffer();
            for (int i=0; i<parkingSlots.size(); i++){
                if (parkingSlots.get(i) != null && parkingSlots.get(i).getRegistrationNumber().equalsIgnoreCase(regNum)){
                    found =true;
                    sb.append(i + " ");
                }
            }
            if (found) System.out.println(sb.toString());
            else System.out.println("Not Found");
        }else System.out.println("Parking lot not found");

    }

    @Override
    public void slotOfParticularColor(String color) {

        if (parkingSlots != null){
            StringBuffer sb = new StringBuffer();
            for (int i=0; i<parkingSlots.size(); i++){
                if (parkingSlots.get(i) != null && parkingSlots.get(i).getColor().equalsIgnoreCase(color)){
                    sb.append(i + " ");
                    found =true;
                }
            }
            if (found) System.out.println(sb.toString());
            else System.out.println("Not found");
        }else System.out.println("Parking lot not found");

    }

    @Override
    public void status() {

        if (parkingSlots != null){
            System.out.println("Slot No.\tRegistration No\tColour");
            for(int i=0; i<parkingSlots.size(); i++){
                if (parkingSlots.get(i) != null){
                    Car currentCar = parkingSlots.get(i);
                    System.out.println(i+"\t"+currentCar.getRegistrationNumber()+"\t"+currentCar.getColor());
                }
            }
        }else System.out.println("Parking lot not found");

    }

    // Just to check
}
