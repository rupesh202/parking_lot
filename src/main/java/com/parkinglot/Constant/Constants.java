package com.parkinglot.Constant;

public class Constants {
    public static final String CREATE_PARKING_LOT = "create_parking_lot";
    public static final String LEAVE = "leave";
    public static final String STATUS = "status";
    public static final String PARK ="park";
    public static final String REG_NUM_OF_CARS_WITH_COLOR = "registration_numbers_for_cars_with_colour";
    public static final String SLOT_OF_CARS_WITH_COLOR ="slot_numbers_for_cars_with_colour";
    public static final String SLOT_OF_REG_NUM ="slot_number_for_registration_number";
}
