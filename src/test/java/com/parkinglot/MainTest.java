package com.parkinglot;

import com.parkinglot.model.Car;
import com.parkinglot.service.ApplicationService;
import com.parkinglot.service.ApplicationServiceImp;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertEquals;

public class MainTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Test
    public void createParkingLot(){
        CaptureOut();
        ApplicationService service = new ApplicationServiceImp();
        service.createParkingLot(4);
        String theOutput = getOut();
        assertEquals("Created a parking lot with 4 slots\n",theOutput);
    }
    @Test
    public void parkACar(){
        CaptureOut();
        ApplicationService service = new ApplicationServiceImp();

        Car car = new Car("KA-AA-1234","Black");
        service.createParkingLot(6);
        service.addCar(car);
        String theOutput = getOut();
        assertEquals("Created a parking lot with 6 slots\nAllocated slot number: 0\n", theOutput);
    }


    public void CaptureOut(){
        System.setOut(new PrintStream(outContent));
    }
    private String getOut(){
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        return outContent.toString().replaceAll("\r","");
    }

}
